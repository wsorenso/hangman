#include "wordGenerator.h"

wordGenerator::wordGenerator()
{
    word = "";
}

string wordGenerator::getWord()
{
    //ctor
    bool good = false;
    srand(time(0));
    int lineNum = 0;
    ifstream fin;
    fin.open("dictionary.txt");
    if(fin.fail())
    {
        cout << "File has been moved or re-named" << endl;
        throw exception();
    }

    if(fin.is_open())
    {
        if(fin.good())
        {
            good = true;
           string placeHolder;
           lineNum = 1000*((rand() % 79343) + 1);// generates a random line number between 1 and the last line in the file mobyposi.txt(oops no it doesn't)
           if(lineNum > 79343) lineNum = lineNum%79343;
           int line = 0;
           for(line = 0; getline(fin, placeHolder, '\n') && line < lineNum; line++)
           {
               if(line == lineNum-1)
               {
                   fin >> word;
               }
            }
        }
    }
    //return good;
    if(good != true)
        cout << "Constructor failed" << endl;

    return word;
}

wordGenerator::~wordGenerator()
{
    //dtor
}

