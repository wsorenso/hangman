#ifndef PAGE_H
#define PAGE_H
#include <string>
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include <iostream>
using namespace std;

class Page
{
    public:
        Page();// init var
        string draw(int nMistakes);// returns a string based on how many bad guess have been made. (Reads a file)
        virtual ~Page();
    protected:
    private:
    string page;// will hold the picture of the hanging man
};

#endif // PAGE_H

