### What is this repository for? ###

* This is a text based console version of the game Hangman. A description of the game including how to play can be found at: https://en.wikipedia.org/wiki/Hangman_(game)

![alt tag](https://bitbucket.org/wsorenso/hangman/raw/master/hangmanCapture.PNG)
### How do I get set up? ###

* To try the game yourself, Clone the repository, cd into the forked directory, and type "make all". This will compile the game and produce an executable called "hangman". To run this type "hangman" (Windows) or "./hangman" (Unix).
* **Do not modify of delete .txt files!**
    The game depends on the configuration of the .txt files, and may not work properly if they are modified.