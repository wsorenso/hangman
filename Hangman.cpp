#include "Hangman.h"

Hangman::Hangman()
{
    reset();
}

void Hangman::play()// contians game logic including guess and draw loop
{
    string ans;
    cout << "Would you like to play Hangman?" << endl;
    cin >> ans;
    while(yes(ans))// play again loop
    {
        cout << "######### HANGMAN ##########" << endl << endl;// game start header
        // game logic here
        while(nFails < 6 && hasWon() == false)
        {
            cout << gPage.draw(nFails+1) << endl;// draw the hangman
            cout << "< " << guesses << " >" << endl;
            cout << "Guess a letter!" << endl;
            cin >> guess;
            if(!contains(guess) && guessRecord.insert(guess).second)// adds guess to a set so player wont lose by guessing the same wrong thing over and over and over and over...
            {
                nFails++;// they guessed wrong
            }
        }
        if(hasWon())
        {
            cout << "Good job, you won!" << endl;
            cout << "The word was: " << word << endl;
        }
        else
        {
            cout << gPage.draw(7);
            cout << "You lose, nice try though..." << endl;
            cout << "The word was: " << word << endl;
        }
        reset();
        cout << "Would you like to play again?" << endl;// check for play again
        cin >> ans;
    }
}

bool Hangman::yes(string ansr)// function returns true if a player wants to play
{
    bool result = false;
    if(ansr != "quit" && ansr != "QUIT" && ansr != "no" && ansr != "NO" && ansr != "No" && ansr != "exit" && ansr != "EXIT"
        && ansr != "Nope" && ansr != "NOPE" && ansr != "nope" && ansr != "End" && ansr != "END" && ansr != "end")
        result = true;

    return result;
}

bool Hangman::contains(char guess)// returns true if the guessed char is in the word
{
    bool result = false;
    for(int i = 0; i < word.length(); i++)
    {
        if(word[i] == guess || word[i] == guess + 32 || word[i] == guess - 32)
        {
            result = true;
            guesses[i] = guess;
        }
    }
    return  result;
}

bool Hangman::hasWon()// returns true if the player has guessed the entire word
{
    bool won = true;
    for(int i = 0; i < guesses.length(); i++)
    {
        if(guesses[i] == '-')// if there are any dashes left in guesses they must not have guessed the word yet
        {
            won = false;
        }
    }
    return won;
}

void Hangman::reset()// reset the game...
{
    guessRecord.erase(guessRecord.begin(), guessRecord.end());// empty the stl set containing the record of their guesses during a game
    word = wGen.getWord();// assigning a random word
    guesses = "";
    nFails = 0;
    for(int i = 0; i < word.length(); i++)
        guesses += '-';// filling guesses with the correct number of dashes

}

Hangman::~Hangman()
{
    //dtor
}

