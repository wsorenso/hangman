#include "Page.h"

Page::Page()
{
    //ctor
    page = "";
}

string Page::draw(int nMistakes)// reads a file to create a string based on nMistakes
{
    int rChecker;//rChecker is used to find that rooms info in the file
    ifstream fin;
    string s1(100, '\n');// 100 newline chars to clear console
    //page = "";
    page = s1;// start out with our newlines
    fin.open("Hangman.txt");
    if(fin.fail())
    {
        throw exception();
    }

    if(fin.is_open())
    {
        if(fin.good())
        {
           string placeHolder;
            while(!fin.eof())//
            {
                getline(fin, placeHolder);// getting a line
                rChecker = atoi(placeHolder.c_str());// checking to see if it starts with a number and what that number is if it does
                if(rChecker == nMistakes)// if current line began with the id num we are looking for...
                {
                    while(placeHolder != "-----")
                    {//read multi-line drawing
                        getline(fin, placeHolder);//getting a line and..
                        if(placeHolder != "-----")
                        {
                            page = page + placeHolder + "\n";
                        }

                    }//END WHILE
                }// END IF
            }// END WHILE !EOF
            fin.seekg(ios::beg);
        }// END GOOD
    }// END OPEN

    return page;
}

Page::~Page()
{
    //dtor
}

