# makefile for the hangman game...

FILES=Page.o wordGenerator.o Hangman.o main.o

debug: $(FILES)
	g++ $(CFLAGS) $(FILES) -o hangman

all: $(FILES)
	g++ $(FILES) -o hangman

CFLAGS=

Page.o: Page.cpp Page.h
	g++ $(CFLAGS) -c Page.cpp

wordGenerator.o: wordGenerator.cpp wordGenerator.h
	g++ $(CFLAGS) -c wordGenerator.cpp

Hangman.o: Hangman.cpp Hangman.h
	g++ $(CFLAGS) -c Hangman.cpp

main.o: main.cpp
	g++ $(CFLAGS) -c main.cpp

clean: 
	rm -f *.o hangman hangman.exe #delete all garbage

delete: clean hangman

debug: CFLAGS=-g
debug: hangman
