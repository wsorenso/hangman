#ifndef WORDGENERATOR_H
#define WORDGENERATOR_H
#include <fstream>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <string>
#include <ctime>
using namespace std;
class wordGenerator// chooses a random word for the computer.
{
    public:
        wordGenerator();// reads a dictionary file and chooses a random word.
        string getWord();// returns the random word chosen
        virtual ~wordGenerator();
    protected:
    private:
    string word;// holds the random word chosen.

};

#endif // WORDGENERATOR_H

