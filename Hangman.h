#ifndef HANGMAN_H
#define HANGMAN_H
#include <string>
#include <set>
#include "wordGenerator.h"
#include "Page.h"

using namespace std;

class Hangman
{
    public:
        Hangman();// fills the parallel strings, initializes vars by calling reset()
        void play();// contains game logic, including guess and draw loop
        bool yes(string ansr);// function returns true if a player wants to play
        bool contains(char guess);// updates guesses and returns true if word has the character guess in it
        bool hasWon();// returns true if the player has guessed the word
        void reset();// resets the Hangman game with a new word etc.
        virtual ~Hangman();
    protected:
    private:
    set<char> guessRecord;
    char guess;
    wordGenerator wGen; // generates the random word
    Page gPage; // draws the hangman
    int nFails;// records the number of wrong guesses.
    string guesses, word;// parallel strings

};

#endif // HANGMAN_H

